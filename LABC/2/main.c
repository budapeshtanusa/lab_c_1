#include <stdio.h>
#include <string.h>

int to_dec(char *in,int base) {
	int res=0;
	int pow=1;
	for(int i=strlen(in)-1;i>=0;i--){
		res=res+(in[i]-'0')*pow;
		pow=pow*base;
	}
	return res;
}

int get_base(char *in) {
	int base=0;	
	base=in[0];
	return base-'0'+1;
}

int main() {
	char b[100];
	char store[100];
	memset(store,0,sizeof(store));
	memset(b,0,sizeof(b));
	printf("input string: ");
	scanf("%100s",b);
	for(int i=0;i<strlen(b);i++){
		if(b[i]>='0'&& b[i]<='9') {
			store[strlen(store)]=b[i];	
		} else {
		       if(strlen(store)>0){
		       		int base=get_base(store);
		 		int char_code=to_dec(store+1,base);
				printf("%c",char_code);
				memset(store,0,sizeof(store));
		       }
		       printf("%c",b[i]);
		}
	}
	if (strlen(store)>0){
		int base=get_base(store);
		int char_code=to_dec(store+1,base);
		printf("%c",char_code);		
	}
	printf("\n");
	return 0;
}
