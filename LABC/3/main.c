#include <stdio.h>
#include <string.h>

int to_dec(char *in,int src_base) {
	int res=0;
	int pow=1;
	for(int i=strlen(in)-1;i>=0;i--){
		res=res+(in[i]-'0')*pow;
		pow=pow*src_base;
	}
	return res;
}

int get_base(char *in) {
	int base=0;
	for(int i=0;i<strlen(in);i++){
		if(in[i]>base){
			base=in[i];
		}
	}
	return base-'0'+1;
}

char int_to_char(int val) {
	switch (val) {
		case 0: return '0';
		case 1: return '1';
		case 2: return '2';
		case 3: return '3';	
		case 4: return '4';	
		case 5: return '5';	
		case 6: return '6';	
		case 7: return '7';	
		case 8: return '8';	
		case 9: return '9';	
		case 10: return 'A';	
		case 11: return 'B';	
		case 12: return 'C';	
		case 13: return 'D';	
		case 14: return 'E';	
		case 15: return 'F';	
	}
}

void revert_string (char* str){
  int len=strlen(str);
  char temp;
  for (int i=0;i<len/2;i++) {
	temp=str[len-i-1];
      	str[len-i-1]=str[i];
	str[i]=temp;
  }
}

int dec_to(char* out,int in,int dst_base) {
	while (in!=0) {
		out[strlen(out)]=int_to_char(in%dst_base);
		in=in/dst_base;
	}
	revert_string(out);
	return 0;
}

void base_to_base(char* out,char* in,int src_base,int dst_base){
	int dec_val=to_dec(in,src_base);
	dec_to(out,dec_val,dst_base);


}

int main() {
	char b[100], a[100];
	memset(a,0,sizeof(a));
	memset(b,0,sizeof(b));
	printf("input first string: ");
	scanf("%100s",a);
	printf("input second string: ");
	scanf("%100s",b);
	printf("\n");
	printf("a=%s b=%s\n",a,b);
	int min_base_a=get_base(a);
	int min_base_b=get_base(b);
	printf("min_base_a=%d\n",min_base_a);
	printf("min_base_b=%d\n",min_base_b);
	char a2[100], b2[100];
	for(int old_a_base=min_base_a;old_a_base<=16;old_a_base++){
		for(int new_a_base=2;new_a_base<=16;new_a_base++){
			memset(a2,0,sizeof(a2));
			base_to_base(a2,a,old_a_base,new_a_base);
			for(int old_b_base=min_base_b;old_b_base<=16;old_b_base++){
				for(int new_b_base=2;new_b_base<=16;new_b_base++){
					memset(b2,0,sizeof(b2));
					base_to_base(b2,b,old_b_base,new_b_base);
					if (strcmp(a2,b2)==0){
						printf("a[%d -> %d] = b[%d -> %d] = %s\n",old_a_base, new_a_base,old_b_base,new_b_base,a2);
					}
				}
			}

		}
	}
	return 0;
}
